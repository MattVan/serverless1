import React, { Component } from 'react';

import axios from 'axios';

// UPDATE WITH YOUR API ENDPOINT
const baseURL ="https://pms0shcakd.execute-api.us-east-1.amazonaws.com/default/library";

class Books extends Component {
  
  constructor(props){
    super(props);
    this.state = {
      books:[],
      authors:[]
    };
    
    this.getAuthorName = this.getAuthorName.bind(this);
  }
  
  componentDidMount(){
    axios.get( baseURL + "/books" )
    .then(result=>{
      console.log(result.data);
      this.setState({
        books: result.data
      });
    })
    .catch(error=>console.log(error));
    
    axios.get( baseURL + "/authors")
    .then(result=>{
      console.log(result.data);
      this.setState({
        authors: result.data
      });
    })
    .catch(error=>console.log(error));
    
  }
  
  addBook(ev) {
    const inputISBN = ev.target.querySelector('[id="isbn"]');
    const isbn = inputISBN.value.trim();

    const inputTitle = ev.target.querySelector('[id="title"]');
    const title = inputTitle.value.trim();

    const inputPages = ev.target.querySelector('[id="pages"]');
    const pages = inputPages.value.trim();
    
    const inputAID = ev.target.querySelector('[id="aid"]');
    const aid = inputAID.value.trim();
    
    console.log( "ISBN: " + isbn );
    console.log( "Title: " + title );
    console.log( "Pages: " + pages );
    console.log( "AID: " + aid);
    
    this.state.books.forEach(book =>{
      if(isbn === book.isbn){
        console.log("A book with that ISBN already exists");
        return;
      }
    });
    
    
    let newbook = {
      isbn,
      title,
      pages,
      aid
    };
    
    axios.post( baseURL + "/books", newbook)
    .then(res => {
      console.log(res);
    });
    
    ev.preventDefault();
  }
  
  deleteBook(isbn, event){
    console.log(isbn);
    axios.delete(baseURL + "/books/" + isbn)
    .then(result => {
      console.log("Deleted");
    })
    .catch(err=>console.log(err));
    
    event.preventDefault();
  }
  
  getAuthorName(id){
    for(let i = 0; i < this.state.authors.length; i++){
      if(parseInt(this.state.authors[i].aid) === parseInt(id)){
        return this.state.authors[i].fName + " " + this.state.authors[i].lName;
      }
    }
  }
  
  render() {

    return(
    <div>
      <div className="py-5 text-center">
        <h2>Books</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.addBook.bind(this)} className="needs-validation" noValidate>
            <div className="row">
              <div className="col-md-3 mb-3">
                <label htmlFor="isbn">ISBN</label>
                <input type="number" className="form-control" id="isbn" defaultValue="1" required />
                <div className="invalid-feedback">
                    An ISBN is required.
                </div>
              </div>

              <div className="col-md-5 mb-3">
                <label htmlFor="title">Title</label>
                <input type="text" className="form-control" id="title" defaultValue="" required />
                <div className="invalid-feedback">
                    A book title is required.
                </div>
              </div>
              
              <div className="col-md-4 mb-3">
              <label htmlFor="author">Author</label>
              <select id="aid" className="form-control">
                {this.state.authors.map(author=>{
                  return <option value={author.aid} key={author.aid}>{author.fName + " "+ author.lName}</option>;
                })}
              </select>
              </div>
              
              <div className="col-md-3 mb-3">
                <label htmlFor="pages">Pages</label>
                <input type="number" className="form-control" id="pages" defaultValue="100" required />
                <div className="invalid-feedback">
                    The number of pages is required.
                </div>
              </div>

            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add book</button>
          </form>
        </div>
      </div>
      
      <table className="table">
        <thead>
          <tr>
            <th scope="col">ISBN</th>
            <th scope="col">Title</th>
            <th scope="col">Pages</th>
            <th scope="col">Author</th>
            <th scope="col">Delete</th>
          </tr>
        </thead>
        <tbody>
          
        {this.state.books && this.state.books.map(book=>
          <tr key={book.isbn}>
          <td>{book.isbn}</td>
          <td>{book.title}</td>
          <td>{book.pages}</td>
          <td>{this.getAuthorName(book.aid)}</td>
          <td><button onClick={this.deleteBook.bind(this, book.isbn)} type="button" className="btn btn-danger">Delete</button></td>
          </tr>)}
        
        </tbody>
      </table>
      
      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default Books;
