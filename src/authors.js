import React, { Component } from 'react';

import axios from 'axios';

// UPDATE WITH YOUR API ENDPOINT
const baseURL ="https://pms0shcakd.execute-api.us-east-1.amazonaws.com/default/library";

class Authors extends Component {
  
  constructor(props){
    super(props);
    this.state = {
      authors:[]
    };
  }
  
  componentDidMount(){
    axios.get( baseURL + "/authors" )
    .then(result=>{
    //   console.log(result.data);
      this.setState({
        authors: result.data
      });
    })
    .catch(error=>console.log(error));
  }

  
  addAuthor(ev) {
    const inputAID = ev.target.querySelector('[id="aid"]');
    const aid = inputAID.value.trim();

    const inputFName = ev.target.querySelector('[id="fName"]');
    const fName = inputFName.value.trim();

    const inputLName = ev.target.querySelector('[id="lName"]');
    const lName = inputLName.value.trim();

    console.log( "AID: " + aid );
    console.log( "First Name: " + fName );
    console.log( "Last Name: " + lName );
    
    this.state.authors.forEach(author =>{
      if(aid === author.aid){
        console.log("An author with that Author ID already exists")
        return;
      }
    });
    
    
    let newAuthor = {
      aid,
      fName,
      lName
    };
    
    axios.post( baseURL + "/authors", newAuthor)
    .then(res => {
      console.log(res);
    });
    
    ev.preventDefault();
  }
  
  render() {

    return(
    <div>
      <div className="py-5 text-center">
        <h2>Authors</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.addAuthor.bind(this)} className="needs-validation" noValidate>
            <div className="row">
              <div className="col-md-2 mb-3">
                <label htmlFor="isbn">Author ID</label>
                <input type="number" className="form-control" id="aid" defaultValue="1" required />
                <div className="invalid-feedback">
                    An Author ID is required.
                </div>
              </div>

              <div className="col-md-5 mb-3">
                <label htmlFor="title">First Name</label>
                <input type="text" className="form-control" id="fName" defaultValue="" required />
                <div className="invalid-feedback">
                    An Author First Name is required.
                </div>
              </div>

              <div className="col-md-5 mb-3">
                <label htmlFor="pages">Last Name</label>
                <input type="text" className="form-control" id="lName" defaultValue="" required />
                <div className="invalid-feedback">
                    An Author Last Name is required.
                </div>
              </div>

            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add Author</button>
          </form>
        </div>
      </div>
      
      <table className="table">
        <thead>
          <tr>
            <th scope="col">Author ID</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Names</th>
          </tr>
        </thead>
        <tbody>
          
        {this.state.authors && this.state.authors.map(author=>
          <tr key={author.aid}>
          <td>{author.aid}</td>
          <td>{author.fName}</td>
          <td>{author.lName}</td>
          
          </tr>)}
        
        </tbody>
      </table>
      
      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default Authors;
